#!/bin/bash
# Testing 02
# Fonction pour incrémenter la version dans un package.json spécifique
bump_version() {
  local DIR=$1
  cd $DIR

  # Incrémenter la version dans package.json
  npm version patch --no-git-tag-version

  # Récupérer la nouvelle version
  VERSION=$(jq -r '.version' package.json)

  cd - > /dev/null
}

# Vérifier si les fichiers package.json ont été modifiés
CHANGED_FILES=$(git diff --name-only HEAD~1 HEAD)

if echo "$CHANGED_FILES" | grep -qE 'VERSION'; then
  echo "Le fichier VERSION à été modifiés. Aucune incrémentation de version nécessaire."
else
  echo "Le fichier VERSION n'a pas été modifiés. Incrémentation de la version..."

  git config --global user.email "shemsasfouri@gmail.com"
  git config --global user.name "shemsasfouri"

  # Lire et incrémenter la version à partir du fichier VERSION
  current_version=$(cat VERSION)
  IFS='.' read -r -a version_parts <<< "$current_version"
  version_parts[2]=$((version_parts[2] + 1))
  new_version="${version_parts[0]}.${version_parts[1]}.${version_parts[2]}"

  echo $new_version > VERSION

  # Incrémenter la version dans les package.json
  bump_version "back"
  bump_version "front"

 # Créer le fichier des notes de version
  RELEASE_NOTE_FILE="release_notes.md"

  echo "Version $new_version" > $RELEASE_NOTE_FILE
  echo "===================" >> $RELEASE_NOTE_FILE
  echo "" >> $RELEASE_NOTE_FILE

  # Récupérer le dernier commit et inclure son nom et son message dans les notes de version
  LAST_COMMIT=$(git log -1 --pretty=%B)
  echo "**Last Commit:**" >> $RELEASE_NOTE_FILE
  echo "$LAST_COMMIT" >> $RELEASE_NOTE_FILE
  echo "" >> $RELEASE_NOTE_FILE

  # Ajouter, commit et push les changements
  git add back/package.json front/package.json VERSION $RELEASE_NOTE_FILE
  git commit -m "Bump version to $new_version and update release notes"
  
  # Créer un tag et le pousser
  git tag -a "v$new_version" -m "Release $new_version"
  git push https://shemsasfouri:${CI_JOB_TOKEN}@gitlab.com/communism1802926/tfoh.git main
  git push https://shemsasfouri:${CI_JOB_TOKEN}@gitlab.com/communism1802926/tfoh.git "v$new_version"
fi
