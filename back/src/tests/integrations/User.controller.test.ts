import request from 'supertest';
import { createServer } from 'http'; // Importez createServer pour créer un serveur HTTP
import app from '../../index';
import UserService from '../../services/User.service';

//Add comement to test Jira integration

// Utilisez le port défini dans la configuration pour vos tests
const PORT = 4000

// Créez un serveur HTTP pour l'application express
let server: any;

// Avant chaque test, démarrez le serveur express sur le port spécifié
beforeEach((done) => {
    server = createServer(app);
    server.listen(PORT, () => {
        console.log(`Test server listening on port ${PORT}`);
        done();
    });
});

// Après chaque test, fermez le serveur
afterEach((done) => {
    server.close(done);
});

// Création des mockups
jest.mock('../../services/User.service', () => ({
  getUserByUsername: jest.fn(),
  getUserByEmail: jest.fn(),
  createUser: jest.fn(), // Ajout de la méthode createUser dans le mockup
}));

describe('Test /api/auth/signup', () => {
    afterEach(() => {
        jest.clearAllMocks(); // Nettoie les mocks après chaque test
    });

    it('Should return 200 and create a new user', async () => {
        // Mockup pour simuler le cas où aucun utilisateur n'existe avec le même nom d'utilisateur ou email
        (UserService.getUserByUsername as jest.Mock).mockResolvedValue(null);
        (UserService.getUserByEmail as jest.Mock).mockResolvedValue(null);
        // Ajout d'un mockup pour la méthode createUser
        (UserService.createUser as jest.Mock).mockResolvedValue({}); // Vous pouvez ajuster la valeur retournée selon vos besoins

        const response = await request(server) // Utilisez le serveur HTTP créé pour les tests
            .post('/api/auth/signup')
            .send({
                email: 'test@example.com',
                password: 'Password1!',
                username: 'testuser',
                securityKey: 'someSecurityKey',
            });

        expect(response.status).toBe(200);
        expect(response.body.message).toBe('User created');
    });

    it('Should return 400 if missing required fields', async () => {
        const userDataWithoutUsername = {
            email: 'test@example.com',
            password: 'Password1!',
            securityKey: 'someSecurityKey'
        }

        const response = await request(server) // Utilisez le serveur HTTP créé pour les tests
            .post('/api/auth/signup')
            .send(userDataWithoutUsername);

        expect(response.status).toBe(400);
        expect(response.body.error).toBe('Missing email, password, username or securityKey');
    });
});