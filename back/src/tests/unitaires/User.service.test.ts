import UserService from "../../services/User.service";
import User from "../../models/User";

// Mock du modèle User
jest.mock('../../models/User', () => {
    return {
        findAll: jest.fn(),
        findByPk: jest.fn(),
        findOne: jest.fn(),
        create: jest.fn(),
        prototype: {
            save: jest.fn()
        }
    };
});

describe('UserService', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    test('should get all users', async () => {
        const users = [
            { id: 1, firstname: 'John', lastname: 'Doe', username: 'johndoe', email: 'john@example.com', password: 'hashedpassword1', date_creation: '20/02/2024', bio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', favorite_jdr: 'Dungeons & Dragons', preference: 'Fantasy', profile_picture: 'url_image_1' },
            { id: 2, firstname: 'Jane', lastname: 'Smith', username: 'janesmith', email: 'jane@example.com', password: 'hashedpassword2', date_creation: '20/02/2024', bio: 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', favorite_jdr: 'Pathfinder', preference: 'Sci-fi', profile_picture: 'url_image_2' }
        ] as User[];
        (User.findAll as jest.Mock).mockResolvedValue(users);

        const result = await UserService.getAllUsers();

        expect(result).toEqual(users);
        expect(User.findAll).toHaveBeenCalledTimes(1);
    });

    test('should get user by ID', async () => {
        const user = { id: 1, username: 'johndoe' } as User;
        (User.findByPk as jest.Mock).mockResolvedValue(user);

        const result = await UserService.getUserById('1');

        expect(result).toEqual(user);
        expect(User.findByPk).toHaveBeenCalledWith('1');
    });

    test('should return null if user by ID is not found', async () => {
        (User.findByPk as jest.Mock).mockResolvedValue(null);

        const result = await UserService.getUserById('1');

        expect(result).toBeNull();
        expect(User.findByPk).toHaveBeenCalledWith('1');
    });

    test('should get user by email', async () => {
        const user = { id: 1, email: 'john@example.com' } as User;
        (User.findOne as jest.Mock).mockResolvedValue(user);

        const result = await UserService.getUserByEmail('john@example.com');

        expect(result).toEqual(user);
        expect(User.findOne).toHaveBeenCalledWith({ where: { email: 'john@example.com'.toLowerCase() } });
    });

    test('should return null if user by email is not found', async () => {
        (User.findOne as jest.Mock).mockResolvedValue(null);

        const result = await UserService.getUserByEmail('john@example.com');

        expect(result).toBeNull();
        expect(User.findOne).toHaveBeenCalledWith({ where: { email: 'john@example.com'.toLowerCase() } });
    });

    test('should get user by username', async () => {
        const user = { id: 1, username: 'johndoe' } as User;
        (User.findOne as jest.Mock).mockResolvedValue(user);

        const result = await UserService.getUserByUsername('johndoe');

        expect(result).toEqual(user);
        expect(User.findOne).toHaveBeenCalledWith({ where: { username: 'johndoe'.toLowerCase() } });
    });

    test('should return null if user by username is not found', async () => {
        (User.findOne as jest.Mock).mockResolvedValue(null);

        const result = await UserService.getUserByUsername('johndoe');

        expect(result).toBeNull();
        expect(User.findOne).toHaveBeenCalledWith({ where: { username: 'johndoe'.toLowerCase() } });
    });

    test('should create a new user', async () => {
        const newUser = { id: 1, email: 'john@example.com', password: 'password', username: 'johndoe', date_creation: '20/02/2024' } as User;
        (User.create as jest.Mock).mockResolvedValue(newUser);

        const result = await UserService.createUser('john@example.com', 'password', 'johndoe', '20/02/2024');

        expect(result).toEqual(newUser);
        expect(User.create).toHaveBeenCalledWith({
            email: 'john@example.com',
            password: 'password',
            username: 'johndoe',
            date_creation: '20/02/2024',
            preference: '-'
        });
    });

    test('should throw an error if user to edit is not found', async () => {
        (User.findByPk as jest.Mock).mockResolvedValue(null);

        await expect(UserService.editUser({ id: 1 } as User)).rejects.toThrow('User not found');
    });

    test('should create user with default preference', async () => {
        const newUser = { id: 1, email: 'john@example.com', password: 'password', username: 'johndoe', date_creation: '20/02/2024', preference: '-' } as User;
        (User.create as jest.Mock).mockResolvedValue(newUser);

        const result = await UserService.createUser('john@example.com', 'password', 'johndoe', '20/02/2024');

        expect(result.preference).toBe('-');
        expect(User.create).toHaveBeenCalledWith(expect.objectContaining({ preference: '-' }));
    });

    test('should throw an error if creating a user with an existing email', async () => {
        (User.create as jest.Mock).mockRejectedValue(new Error('Validation error'));

        await expect(UserService.createUser('john@example.com', 'password', 'johndoe', '20/02/2024')).rejects.toThrow('Validation error');
    });

    test('should handle error during user retrieval by ID', async () => {
        (User.findByPk as jest.Mock).mockRejectedValue(new Error('Database error'));

        await expect(UserService.getUserById('1')).rejects.toThrow('Database error');
    });

    test('should handle error during user creation', async () => {
        (User.create as jest.Mock).mockRejectedValue(new Error('Database error'));

        await expect(UserService.createUser('john@example.com', 'password', 'johndoe', '20/02/2024')).rejects.toThrow('Database error');
    });
});
