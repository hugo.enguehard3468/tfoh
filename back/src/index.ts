import express from 'express';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import path from 'path';

import authRoutes from './routes/Auth.routes';
import userRoutes from './routes/User.routes';
import characterRoutes from './routes/Character.routes';

require('dotenv').config()

const app = express();
const PORT = process.env.NODE_ENV === 'test' ? 4000 : parseInt(process.env.PORT || '3004', 10);
const corsOptions = {
  origin: 'http://localhost:3000',
  credentials: true,
}

const userImagesPath = path.resolve(__dirname, 'user-images/pp');

app.use(cookieParser());
app.use(express.json());
app.use(cors(corsOptions));

app.use('/user-images/pp', express.static(userImagesPath));

app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/characters', characterRoutes);

if (process.env.NODE_ENV !== 'test') {
  app.listen(PORT, () => {
    console.log(`API started on http://localhost:${PORT}/api/`);
  });
}

export default app;