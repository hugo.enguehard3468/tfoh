import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { TestPageComponent } from './components/TestPage/Test-page';

test('renders the correct content based on isShow prop', () => {
  const handleButtonClick = jest.fn();
  
  // Render the component with isShow = false
  const { rerender } = render(<TestPageComponent handleButtonClick={handleButtonClick} isShow={false} />);
  
  // Check that "Marika" is displayed
  expect(screen.getByText('Marika')).toBeInTheDocument();
  expect(screen.queryByText('Radagon')).not.toBeInTheDocument();
  
  // Render the component with isShow = true
  rerender(<TestPageComponent handleButtonClick={handleButtonClick} isShow={true} />);
  
  // Check that "Radagon" is displayed twice
  const radagonElements = screen.getAllByText('Radagon');
  expect(radagonElements).toHaveLength(2);
  expect(screen.queryByText('Marika')).not.toBeInTheDocument();
});

test('calls handleButtonClick when the button is clicked', () => {
  const handleButtonClick = jest.fn();
  
  render(<TestPageComponent handleButtonClick={handleButtonClick} isShow={false} />);
  
  // Simulate button click
  fireEvent.click(screen.getByText('Test'));
  
  // Check that handleButtonClick was called
  expect(handleButtonClick).toHaveBeenCalledTimes(1);
});
