export default interface SignupData {
    username: string,
    email: string,
    password: string,
}