export default interface SignupUserResult {
    response?: boolean,
    message?: string,
}