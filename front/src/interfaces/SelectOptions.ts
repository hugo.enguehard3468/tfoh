export default interface SelectOptions {
    id: string,
    label: string,
}