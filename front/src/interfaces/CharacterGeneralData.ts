export default interface CharacterGeneralData
{
    id: string,
    isFavorite: boolean,
    image: string,
    name: string,
    campaign: string,
    date_creation: string,
}