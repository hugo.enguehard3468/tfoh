export default interface SignupForm {
    username: string,
    password: string, 
    confirmPassword: string,
    email: string,
    acceptCGU: boolean,
}