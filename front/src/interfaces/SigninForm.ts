export default interface SigninForm {
    username: string,
    password: string,
}