import { useState } from "react";
import { TestPageComponent } from "../components/TestPage/Test-page";


export const TestPageContainer = () => {
    const [isShow, setIsShow] = useState<boolean>(false);
    const handleButtonClick = () => {
        setIsShow(!isShow);
    }

    return (
        <TestPageComponent handleButtonClick={handleButtonClick} isShow={isShow} />
    );
}