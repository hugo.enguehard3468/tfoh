// React imports
import { ProfileContainer } from "../../containers/Profile.container";

export const ProfilePage = () => {
    return (
        <ProfileContainer />
    );
}