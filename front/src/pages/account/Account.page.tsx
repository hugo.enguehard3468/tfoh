// React imports
import { AccountContainer } from "../../containers/Account.container";

export const AccountPage = () => {    
    return (
        <AccountContainer />
    );
}