// Material imports
import styled from "@emotion/styled";
import { Box, Grid, Typography } from "@mui/material";



export const CustomTypographyText = styled(Typography)({
    fontSize: '20px',
});
