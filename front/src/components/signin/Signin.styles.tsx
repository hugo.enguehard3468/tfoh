import { styled, Typography } from "@mui/material";

export const CustomTypographyText = styled(Typography)({
    fontSize: '20px',
});