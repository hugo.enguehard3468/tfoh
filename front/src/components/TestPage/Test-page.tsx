interface TestPageComponentProps {
    handleButtonClick: () => void;
    isShow: boolean;
}
export const TestPageComponent = ({
    handleButtonClick,
    isShow
}: TestPageComponentProps) => {
    return(
        <div>
            <button onClick={handleButtonClick}>Test</button>
            <p>{isShow?(
                <>
                <span className="radagon">Radagon</span>
                <br />
                <span className="radagon">Radagon</span>
                </>
            ):(
                <span className="marika">Marika</span>
            )}</p>
        </div>
    )
}