// Material imports
import { Box, Input, styled } from "@mui/material";


export const CustomBox = styled(Box)({
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '10px',
    margin: '10px 0',
});

export const CustomInput = styled('input')({
    width: '20px',
    height: '20px',
    marginRight: '10px',
});